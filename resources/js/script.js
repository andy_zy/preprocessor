var onEvent = function(elem, event, callback){
	elem.addEventListener(event, callback, false);
};

onEvent(document, 'DOMContentLoaded', function(){
	var html = document.documentElement,
		processor = document.querySelector('.processor'),
		elemVarInputs = document.querySelectorAll('[name="elName"], [name="varName"]'),
		styleTechnologyInput = document.querySelector('[name="styleSrcType"]'),
		styleTechnologyOpts = document.querySelectorAll('.style-type .technology'),
		watchModeInput = document.querySelector('.watch input'),
		watchModeDisableBtn = document.querySelector('.watch-mode-disable a'),
		onMessage = function(data){
			var msgBox = document.querySelector('#message-box'),
				serializeMessage = function(msg){
					return (msg || '').replace(/\n/g, '<br>');
				};

			msgBox.className = data.status;
			msgBox.innerHTML = '<span>' + serializeMessage(data.msg) + '</span>';
		},
		serializeFormData = function(form){
			var elements = form.querySelectorAll('[name]'),
				data = {
					raw: {},
					store: {}
				};

			[].forEach.apply(elements, [function(el){
				if( el.type === 'radio' ){
					if( el.checked ){
						data.raw[el.name] = el.value;
						data.store['[name="' + el.name + '"][value="' + el.value + '"]'] = el.checked;
					}
				}
				else{
					data.raw[el.name] = el.type === 'checkbox' ? el.checked : el.value;
					data.store['[name="' + el.name + '"]'] = el.type === 'checkbox' ? el.checked : el.value;
				}
			}]);

			return data;
		},
		changeInputsState = function(rmAttr, addAttr){
			for(var i = 0; i < elemVarInputs.length; i++){
				elemVarInputs[i].removeAttribute(rmAttr);
				elemVarInputs[i].setAttribute(addAttr, true);
			}
		},
		storeData = function(data){
			localStorage.setItem('preprocessor', JSON.stringify(data));
		},
		restoreData = function(){
			var data = JSON.parse(localStorage.getItem('preprocessor') || '""');

			if( data ){
				for(var key in data){
					if( data.hasOwnProperty(key) ){
						var item = document.querySelector(key);

						if( /checkbox|radio/.test(item.type) ){
							item.checked = data[key];

							if( /watch/.test(key) &&  data[key] ){
								changeInputsState('required', 'disabled');
							}
						}
						else{
							item.value = data[key];
						}

						if( /styleSrcType/.test(key) ){
							data[key].split(',').forEach(function(value){
								(document.querySelector('.style-type .technology[value="' + value + '"]') || {}).click();
							});
						}
					}
				}
			}
		},
		sendData = function(data){
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function(){
				if( this.readyState == 4 && this.status == 200 ){
					onMessage(JSON.parse(this.responseText));

					if( !watchModeInput.checked ){
						html.classList.remove('processing');
					}
				}
			};

			xhr.onerror = function(){
				onMessage({
					status: 'error',
					msg: 'SERVER ERROR. RESTART THE SERVER'
				});
			};

			xhr.open('POST', '/', true);
			xhr.send(JSON.stringify(data));
		},
		disableWatchMode = function(){
			if( watchModeInput.checked && ~[].slice.apply(html.classList).indexOf('watch-enabled') ) {
				new Image().src = '/exit?=' + Math.round(Math.random() * 10e6);
			}
		};

	[].forEach.apply(styleTechnologyOpts, [function(el){
		onEvent(el, 'change', function(){
			var checkedOpts = [];

			[].forEach.apply(document.querySelectorAll('.style-type .technology:checked'), [function(chkd){
				checkedOpts.push(chkd.value);
			}]);

			styleTechnologyInput.value = checkedOpts.join(',');
		});
	}]);

	onEvent(watchModeInput, 'change', function () {
		if( watchModeInput.checked ){
			changeInputsState('required', 'disabled');
		}
		else{
			changeInputsState('disabled', 'required');
		}
	});

	onEvent(processor, 'submit', function(e){
		var data = serializeFormData(this);

		e.preventDefault();

		if( !styleTechnologyInput.value ){
			return onMessage({
				status: 'error',
				msg: 'At least one source must be selected!'
			});
		}

		if( watchModeInput.checked ){
			html.classList.add('watch-enabled');

			onMessage({
				status: 'ok',
				msg: 'Watch mode is enabled!'
			});
		}

		html.classList.add('processing');
		storeData(data.store);
		sendData(data.raw);
	});

	onEvent(watchModeDisableBtn, 'click', function(){
		disableWatchMode();

		onMessage({
			status: 'error',
			msg: 'Watch mode is disabled!'
		});

		html.classList.remove('watch-enabled');
		html.classList.remove('processing');
	});

	onEvent(window, 'beforeunload', disableWatchMode);

	restoreData();
});